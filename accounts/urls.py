from django.conf.urls import patterns, url
from django.contrib.auth.views import login, logout

from accounts import views

urlpatterns = patterns('',
	url(r'^login$', login, {"template_name":"gameshop/index.html"}),
	url(r'^logout$', views.logout),
	url(r'^register$', views.register),
	url(r'^success$', views.register_success),
	url(r'^confirm/(?P<activation_key>\w+)/', views.register_confirm),
)