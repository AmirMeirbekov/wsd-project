from django.contrib import auth
from django.http import HttpResponseRedirect, HttpResponse
from django import forms
from django.shortcuts import render, render_to_response, get_object_or_404
from accounts.forms import *
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import Group

from django.template import RequestContext, loader

from accounts.models import UserProfile

import hashlib, datetime, random
from django.utils import timezone
from django.core.mail import send_mail

### Accounts app views.

def login(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        # Correct password, and the user is marked "active"
        auth.login(request, user)
        # Redirect to a success page.
        return HttpResponseRedirect('/', {'user': request.user})
    else:
        # Show an error page
        return HttpResponseRedirect('/', {'error_message': 'nooo'})

def logout(request):
    auth.logout(request)
    return HttpResponseRedirect('/')

@csrf_protect
def register(request):
    #if this is a POST request we need to process the form data
    if request.method == 'POST':
        #create a form instance and populate it with data from the request
        form = RegistrationForm(request.POST)
        #check whether it's valid
        if form.is_valid():
            #process the data in form.cleaned as required      

            username=form.cleaned_data['username']
            email = form.cleaned_data['email']
            string = str(random.random())
            salt = hashlib.sha1(string.encode('utf8')).hexdigest()[:5] 
            salt_email = salt+email           
            activation_key = hashlib.sha1(salt_email.encode('utf8')).hexdigest()            
            key_expires = datetime.datetime.today() + datetime.timedelta(2)

            #Send email with activation key
            email_subject = 'Account confirmation'
            email_body = "Hey %s,\n" \
                         "Thanks for signing up. To activate your account in Game Platform, click this link within 24 hours:\n" \
                         "http://cryptic-harbor-9866.herokuapp.com/accounts/confirm/%s" % (username, activation_key) 

            send_mail(email_subject, email_body, 'wsdlarproject@gmail.com', [email], fail_silently=False)

            user = User.objects.create_user(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password1'],
                email=form.cleaned_data['email'],
            )
            if form.cleaned_data['group']=='D':
                group = Group.objects.get(name='developer')
                user.groups.add(group)
            user.is_active = False     
            user.save()

            # Create and save user profile 
            new_profile = UserProfile(user=user, activation_key=activation_key, key_expires=key_expires)
            new_profile.save()
            #redirect to a new URL:
            return HttpResponseRedirect('/accounts/success')

    #if a GET (or any other method) we'll create a blank form
    else:
        form = RegistrationForm()
    template = loader.get_template('accounts/register.html')
    context = RequestContext(request, {
            'form':form
        })
    return HttpResponse(template.render(context))

def register_success(request):
    template = loader.get_template('accounts/register_success.html')
    context = RequestContext(request, {

        })
    return HttpResponse(template.render(context))

def register_confirm(request, activation_key):
    #check if user is already logged in and if he is redirect him to some other url, e.g. home
    if request.user.is_authenticated():
        HttpResponseRedirect('/')

    # check if there is UserProfile which matches the activation key (if not then display 404)
    user_profile = get_object_or_404(UserProfile, activation_key=activation_key)

    #check if the activation key has expired, if it hase then render confirm_expired.html
    if user_profile.key_expires < timezone.now():
        message = "Your activation key has expired"
        template = loader.get_template('gameshop/sorry_page.html')
        context = RequestContext(request, {
                'message':message,
            })
        return HttpResponse(template.render(context))
    #if the key hasn't expired save user and set him as active and render some template to confirm activation
    user = user_profile.user
    user.is_active = True
    user.save()
    message = "Congratulations, you have been successfully activated your account!"
    template = loader.get_template('accounts/register_success.html')
    context = RequestContext(request, {
            "message": message,
        })
    return HttpResponse(template.render(context))
