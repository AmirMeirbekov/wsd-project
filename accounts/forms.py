import re
from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

class RegistrationForm(forms.Form):
	GROUPS = (
		('D','Developer'),
		('P','Player'),
	)
	username = forms.CharField(label="Nickname", max_length = 100, required=True, widget=forms.TextInput(attrs={'class':'form-control'}))
	email = forms.EmailField(widget=forms.TextInput(attrs={'class':'form-control', 'required':'True', 'max_length':'30'}), label=_("Email address"))
	password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'required':'True', 'max_length':'30', 'render_value':'False'}), label=_("Password"))
	password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'required':'True', 'max_length':'30', 'render_value':'False'}), label=_("Repeat password"))
	group = forms.ChoiceField(choices=GROUPS, widget=forms.RadioSelect())
 
	def clean_username(self):
		try:
		    user = User.objects.get(username__iexact=self.cleaned_data['username'])
		except User.DoesNotExist:
		    return self.cleaned_data['username']
		raise forms.ValidationError(_("The username already exists. Please try another one."))

	def clean(self):
		if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
		    if self.cleaned_data['password1'] != self.cleaned_data['password2']:
		        raise forms.ValidationError(_("The two password fields did not match."))
		return self.cleaned_data

	def clean_email(self):
		try:
		    email = User.objects.get(email__iexact=self.cleaned_data['email'])
		except User.DoesNotExist:
		    return self.cleaned_data['email']
		raise forms.ValidationError(_("The username with such email already exists. Please try another email."))