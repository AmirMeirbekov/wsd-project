from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader

from gameshop.models import Games, PlayerInventory

from datetime import datetime
from hashlib import md5

from django.core.exceptions import ObjectDoesNotExist

### Payment app views.
def buyGame(request):
	if request.user.is_authenticated():
		if request.GET and request.GET['id']:
			game_id = request.GET['id']
			if request.user.has_perm('gameshop.can_add') and Games.dev_has_game(request.user, game_id): #developer can't play his own games
				message = "You can't play your own games"
				template = loader.get_template('gameshop/sorry_page.html')
				context = RequestContext(request, {
						"message": message,
					})
				return HttpResponse(template.render(context))
			if PlayerInventory.has_game(request.user, game_id): #player can't play games he already has in inventory
				message = "You have already bought this game"
				template = loader.get_template('gameshop/sorry_page.html')
				context = RequestContext(request, {
						"message": message,
					})
				return HttpResponse(template.render(context))
			try: #check if game exists in database
				game = Games.objects.get(pk=game_id)
				#creating a cheksum for payment validation
				checksumstr = "pid=%s&sid=%s&amount=%s&token=%s" % (game.id, 'larwsdkey', game.game_price,'810c33ef3b98f66e7fe14896abbaff72')
				m = md5(checksumstr.encode("ascii"))
				checksum = m.hexdigest()

				template = loader.get_template('payment/payment.html')
				context = RequestContext(request, {
						"game":game, 
						"checksum":checksum,
					})
				return HttpResponse(template.render(context))
			except ObjectDoesNotExist:
				message = "There is no such game in database."
				template = loader.get_template('gameshop/sorry_page.html')
				context = RequestContext(request, {
						'message':message,
					})
				return HttpResponse(template.render(context))	
		else:
			message = "Some error with GET request occured. Please try again"
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
				})
			return HttpResponse(template.render(context))	
	else:
		message = "You have to login to buy games"
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
			})
		return HttpResponse(template.render(context))

def success(request):
	if request.user.is_authenticated() and request.GET:
		pid = request.GET['pid']
		ref = request.GET['ref']
		checksumstr = "pid=%s&ref=%s&token=%s" % (pid, ref, '810c33ef3b98f66e7fe14896abbaff72')
		m = md5(checksumstr.encode("ascii"))
		checksum = m.hexdigest()

		if checksum == request.GET['checksum']: #check via checksum if the payment refenrence is right
			game_purchased = Games.objects.get(pk=pid)
			game_purchased.game_number_downloads += 1
			game_purchased.game_last_download = datetime.now()
			game_purchased.save()
			inventory_item = PlayerInventory(
					player=request.user,
					game=game_purchased,
					order_num = ref,
				)
			inventory_item.save()
			template = loader.get_template('payment/success.html')
			context = RequestContext(request, {
					"game":game_purchased, 
					"ref":ref,
				})
			return HttpResponse(template.render(context))
		else:
			message = "You are cheating us!!!"
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					"message": message,
				})
			return HttpResponse(template.render(context))
	else:
		message = "You are not supposed to be here"
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				"message": message,
			})
		return HttpResponse(template.render(context))

def addFreeGame(request):
	if request.user.is_authenticated() and request.GET['id']:
		game_id = request.GET['id']
		if request.user.has_perm('gameshop.can_add') and Games.dev_has_game(request.user, game_id): #developer can't play his own games
			message = "You can't play your own games"
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					"message": message,
				})
			return HttpResponse(template.render(context))
		if PlayerInventory.has_game(request.user, game_id): #developer can't play his own games
			message = "You have already have this game in your inventory"
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					"message": message,
				})
			return HttpResponse(template.render(context))
		try: 
			game = Games.objects.get(pk=game_id)
		except ObjectDoesNotExist:
			message = "There is no such game in database."
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
				})
			return HttpResponse(template.render(context))	
		if game.game_price:
			return HttpResponseRedirect('/payment/buyGame?id={{game.id}}')
		else:
			game.game_number_downloads += 1
			game.game_last_download = datetime.now()
			game.save()
			inventory_item = PlayerInventory(
					player=request.user,
					game=game,
				)
			inventory_item.save()
			template = loader.get_template('payment/success_free_game_added.html')
			context = RequestContext(request, {
					"game":game, 
				})
			return HttpResponse(template.render(context))		
	else:
		message = "Unable to add game to your inventory. Please try again later."
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				"message": message,
			})
		return HttpResponse(template.render(context))

def error(request):
	template = loader.get_template('payment/error.html')
	context = RequestContext(request, {
			#page does not have any connection with database
		})
	return HttpResponse(template.render(context))

def cancel(request):
	template = loader.get_template('payment/cancel.html')
	context = RequestContext(request, {
			#page does not have any connection with database
		})
	return HttpResponse(template.render(context))