from django.conf.urls import patterns, url

from payment import views

urlpatterns = patterns('',
	url(r'^buyGame$', views.buyGame),
	url(r'^success$', views.success),
	url(r'^error$', views.error),
	url(r'^cancel$', views.cancel),
	url(r'^addFreeGame$', views.addFreeGame),
)