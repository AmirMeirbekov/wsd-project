# Wsd-project - JSgames webshop

link: http://cryptic-harbor-9866.herokuapp.com

## Team
Roman Filippov 464455 - mobile applications development background, a little bit of web-development.
Amir Meirbekov 465810 - some Yii (PHP framework) development background. 
Lizaveta Staravoitava 467135- a little bit of frontend-development.

## Features implemented
The following features were implemented. Our assessments for their grading are presented below each feature.

### Mandatory features
Minimum functional requirements

*   Register as a player and developer.
*   As a developer: add games to their inventory, see list of game sales, play games in the same way as a player.
*   As a player: buy games, play games, see game high scores and record their score to it.

Authentication: 

*   Django authentication was used
*   Login as a developer and player
*   Player can only play games he purchased.
*   Developer can add games only to his inventory. Developer can also be a player. Developer can not play his own games.
*   Email validation implemented.
*   For realization of above mentioned features Django's groups and permissions functionality was used - 'developer' group was created and 'can_add' permission for that group was added.
*   **Grade assessment: 200 points.**  

Basic player functionalities:

*   Payment service implemented
*   Game interaction implemented
*   Categories for games implemented  
*   Removing games from inventory implemented
*   **Grade assessment: 250 points.**
	*Comments:* Search by typing functionality has not been implemented.

Basic developer functionalities:

+   Adding game includes - provision of game name, game description, setting price, adding url, selecting category.
+   Game can be removed and game details can be modified.
+   Simple presentation of sales and inventory statistics implemented.
+   **Grade assessment: 200 points.**

Game/service interaction:

*   All game interaction functionality was implemented. For more details see Game development chapter.
*   **Grade assessment: 200 points.**

Quality of work:

*   **Grade assessment: 30 points.**
	*Comments:* 
	*  Template system is well designed and flexible. Model-View-Template separation of concerns was followed
	*  Scarce of comments
	*  Dry-Repeat priciple perhaps is not fully followed - perhaps, check for allowance of access in the views was not written efficiently
	*  Inventory/sales statistics can be improved
	*  Change password, account details functionality was not implemented

### More Features

Save/load feature:

*   Implemented
*   **Grade assessment: 100 points.**

3rd party login (0-100 points):

*   not implemented
*   **Grade assessment: 0 points.**

RESTful API (0-100 points):

*   not implemented
*   **Grade assessment: 0 points.**

Own game:

*   developed
*   **Grade assessment: 100 points.**

Mobile Friendly: 

*   Bootstrap was used
*   Web-site is responsive
*   **Grade assessment: 50 points.**

Social media sharing:

*   Share on Facebook implemented
*   **Grade assessment: 50 points.**

Non-functional requirements (0-200 points):

*	*to be assessed*
*   **Grade assessment: points.**

**Total:** 1180

## Web-site structure

### Apps

Web-site consists of three components (apps): gameshop, accounts, payment. For user authentication the Django's user authentication/authorization interface is used.

**0.User authentication system.** To distinguish users between 'developers' and 'gamers', the user group 'developer' was created with permission codename 'can_add'. When user is being registered, this group assigned to new user if required (i.e. if user is being registered as a developer).

**1.Gameshop** - is the main app, which represents the wrap-up for the whole web-site. 

*Gameshop's models:*

*   Games - handles all information on a game uploaded by developer, including game name, game description, game link, publication date, number of downloads, game price, game category, last download, reference to game developer who uploaded the game.
Games model has the following methods, which are described in more detail in models file itself (models.py): get_categories, get_category, dev_has_game.
Gameshop app doesn't have separate model for handling games categories. Instead, game categories are kept as a tuple in game model (CATEGORY_NAMES).

*   PlayerInventory - handles all information on user's purchased games, including reference to the game, reference to the player, game state (if user saves game), max score, order number
PlayerInventory model has the following methods, which are described in models file itself (models.py): get_top_scores, has_game.

*Gameshop's views:*

*   Gameshop views are described in view file itself (views.py).

*Gameshop's forms:*

*   For working with forms, gameshop app has the file forms.py. Gameshop has only one form to handle - adding new games. For more details on handling the form see forms.py file. 

**2.Accounts** - is the app responsible for managing authorization and user registration issues.

*Accounts's models:*

*   Accounts app doesn't have any models as it uses Django's user authentication inteface.

*Accounts's views:*

*   Accounts views are described in view file itself (views.py).

*Accounts's forms:*

*   For working with forms, accounts app has the file forms.py. Accounts has only one form to handle - registering new users. For more details on handling the form see forms.py file. 

**3.Payment** - is the app responsible for interaction with payment system and managing payment procedure from the payment initiating till accepting and registerig it.

*Payment's models:*

*   Payment app doesn't have any models as it uses Django's user authentication inteface.

*Payment's views:*

*   Payment views are described in view file itself (views.py).

*Payment's forms:*

*   Payment app has only one form which is doesn't have association with a model. The form is created manually in payment.html file.


### Template structure

Django's template logic was used through the development. 
Core wrapping htmls are __base.html and __standard_content.html

*   __base.html contains all connections to external libraries, css and js files.  Also it contains header and footer for the whole website.
*   __standard_content.html extends __base.html and was aimed to change __base.html file if required. However in that project __standard_content.html adds only navigation links to the header.
*   all other html files extends __standard_content.html
*   template's block approach was used through on all pages

### Security issues

Each view has certain access rules. 'index' and 'about' view don't need authorization. All other pages requre authorization.
In addition the following restrictions placed:
	- Developer can't play his own games.
	- Player can't buy games twice.
Security against manually typing any url path is implemented.

### Game development

The game is a realization of an old game “Snake”.
It is written in javascript and consists of game logic and drawing modules files - HTML file with canvas and JS file with drawing procedures and game logic functions. 

The game interacts with the main application through post-messages. When a player presses SAVE/LOAD button, the game sends a post-message with type “SAVE” or “LOAD_REQUEST” respectively to a parent window. 
In case of “SAVE” the message contains essential game state parts - array of snake cells with coords, current score and last direction of snake movement. In case of “LOAD_REQUEST” the message is an empty request for the data. This triggers corresponsing actions in the application itself to get game state from database and send it back to the game.
The game also can submit new high scores to the main application with post-message of type “SCORE”, which contains current score value. 

When the main applications receives a “LOAD”/“SAVE” type of message, it communicates with application through AJAX requests to save/load data in/from database.

### Customer side documentation

Service supports three types of users: registered players, registered developers, guests. Guests can browse a homepage with games, categories, as well as check game information including top results. They need to proceed with registration and create an account in order to buy or add a game. Registred players in addition to guest functionality can also manage a personal account, buy/remove a game from its inventory, view table of personal results, share the results via Facebook. Developers in addtion to registered players functionality can also add own games, edit game details, remove games, see game statistics.

Screenshots of each page with possible actions are presented below. 

![Home page](/images/home.png?raw=true)
The home page displays available games, categories for easy search. A user can sign up or log in both with Facebook or a simple registration form.

![About page](/images/about.png?raw=true)
From the navigation panel the user can go to About page to read a service-related  information and team contacts. 

Registration page. 
The user is redirected to the page from the main page in case he want to create an account. In order to create the account all fields should be filled in. The user can be registered as a developer or as a player. 
![Rigistration page](/images/registration.png?raw=true)

After the user has pressed the submit button, he receives a notification about a successful registration. 
![Notification page](/images/notification.png?raw=true)

The user can go to the main page (Home page) to proceed with the service and log in.After entering a correct username and password the user (player in this case) gets an access to his account.
![Home page](/images/home_reg.png?raw=true)

The user can see his purchased games, top results, account settings. 
![Gamer. Account page](/images/gamer_page.png?raw=true)

The user can play a game and see high scores.
![Game description](/images/game_descr.png?raw=true) 
![Gameplay](/images/gameplay.png?raw=true)
There are options Save game and Load game, as well as a basic information about a game: name, current scores.  

A developer has his own account with both his own games and bought games. Simple statistics is available from the main page of his account. 
![Developer. Account page](/images/developer_page.png?raw=true)

Apart all available actions, the developer has some advanced functions. One of them is to add a new game. 
![Add game](/images/add.png?raw=true)

Moreover the developer can edit a game description and check advanced statistics from the game description view. 
![Game description](/images/description.png?raw=true)


## Development process

First we analysed the scope of the project and skills of team members. All functional parts (backend, frontend, game development and interaction with a service) were distributed between team members. Furthermore, the timeplan was created to meet all deadlines. In order to fulfill all requirements and ensure a high-quality service we tested all features and functions separately and the whole service in the end. According to a plan we would have time to make corrections. Moreover, we should keep in mind early stage test deployment to Heroku.

We created a preliminary structural URL in order to gain a clear understanding of which views we should implement and design interaction between users and the service. 
![Prelimimary structural URL](/images/structural_URL.png?raw=true)

For frontend as well as for better discussions about features within the team we created sketched mockups. 

Unfortunately, we did not complete some additional features we had planned. At this point we aim at better quality and reliability of mandatory features.

Even though our team has limited programming skills we managed to implement all basic features. Moreover, we managed to establish a smooth communication within the team and do everything on time. We tried to make an extensive documentation from three perspectives: project side, customer side and developer side, which required different analytical approaches. We are quite satisfied with our end-product.
