from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'^$', include('gameshop.urls')),
    # url(r'^blog/', include('blog.urls')),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^gameshop/', include('gameshop.urls')),
    url(r'^payment/', include('payment.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
