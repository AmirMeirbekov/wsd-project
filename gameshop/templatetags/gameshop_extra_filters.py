from django import template

register = template.Library()

@register.tag
def multiply(value, arg):
    return (value) * arg