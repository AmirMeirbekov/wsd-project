from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

# Create your models here.

class Games(models.Model):
	CATEGORY_NAMES = (
			('L', 'Logical'),
			('S', 'Strategies'),
			('B', 'Board games'),
		)
	game_name = models.CharField(max_length=50, blank=False)
	game_desc = models.TextField(blank=False)
	game_link = models.CharField(max_length=200, blank=False)
	game_pub_date = models.DateField(auto_now_add=True)
	game_number_downloads = models.IntegerField(default=0)
	game_price = models.FloatField(blank=False)
	game_last_download = models.DateField(auto_now=False, editable=True, null=True)
	game_category = models.CharField(max_length=1, choices=CATEGORY_NAMES)
	game_developer = models.ForeignKey(User)

	def __str__(self):
		return self.game_name

	@classmethod
	def get_categories(self):
		return self.CATEGORY_NAMES

	@classmethod
	def get_category(self, category_ref):
		for category in self.CATEGORY_NAMES:
			if category_ref == category[0]:
				return category[1]

	@classmethod
	def dev_has_game(self, user, game_id):
		try:
			has_game = self.objects.get(pk=game_id, game_developer=user)
		except ObjectDoesNotExist:
			return False
		return True	

	@classmethod
	def get_free_games(self):
		list_of_free_games = Games.objects.filter(game_price=0)
		return list_of_free_games				

class PlayerInventory(models.Model):
	player = models.ForeignKey(User)
	game = models.ForeignKey(Games)
	game_state = models.TextField(null=True, blank=True)
	order_num = models.IntegerField(null=True, blank=True)
	max_score = models.FloatField(default=0, blank=True)

	@classmethod
	def get_top_scores(self, request, game_id, number_of_items=0):
		if number_of_items != 0:
			games = self.objects.filter(game=game_id).order_by('-max_score')
			if games.count() > number_of_items:
				return games[0:number_of_items]
			else:
				return games
		else:
			games = self.objects.filter(game=game_id).order_by('max_score')
			return games
	
	@classmethod
	def has_game(self, user, game_id):
		try:
			has_game = self.objects.filter(game=game_id)
			try:
				has_game = has_game.get(player=user)
				return True
			except ObjectDoesNotExist:
				return False
		except ObjectDoesNotExist:
			return False
