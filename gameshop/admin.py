from django.contrib import admin
from gameshop.models import Games, PlayerInventory
# Register your models here.

admin.site.register(Games)
admin.site.register(PlayerInventory)
