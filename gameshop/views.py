from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader

from gameshop.forms import *
from gameshop.models import Games, PlayerInventory

from django.views.decorators.csrf import csrf_protect
from datetime import datetime
import json

from django.core.exceptions import ObjectDoesNotExist

###Gameshop app views.

def index(request): #represents Home Page of the web-site.
	if request.GET.get('category'): #in case if a user wants to see games under specific category
		category = request.GET.get('category')
		games = Games.objects.filter(game_category=category)
	else: #show all games available if no category selected
		category = ''
		games = Games.objects.all()
	template = loader.get_template('gameshop/index.html')
	context = RequestContext(request, {
			"games_list":games,
			"user": request.user,
			"categories": Games.get_categories(),
			"category_ref": category, #no category selected by user
			"free_games": Games.get_free_games(),
		})
	return HttpResponse(template.render(context))

def about(request): #represents About Page of the web-site.
	template = loader.get_template('gameshop/about.html')
	context = RequestContext(request, {
			# does not have any interacation with database
		})
	return HttpResponse(template.render(context))

@csrf_protect
def addGame(request): #shows addGame interface and handles game addition to the database
	if request.user.is_authenticated():
		if request.user.has_perm('gameshop.can_add'):
			if request.method == 'POST':
			    #create a form instance and populate it with data from the request
			    form = AddGameForm(request.POST)
			    #check whether it's valid
			    if form.is_valid():#process the data in form.cleaned as required
			        #saving added game
			        new_game = Games(
			            game_name=form.cleaned_data['game_name'],
			            game_desc=form.cleaned_data['game_desc'],
			            game_link=form.cleaned_data['game_link'],
			            game_category=form.cleaned_data['game_category'],
						game_price=form.cleaned_data['game_price'],
			            game_pub_date=datetime.now(),
						game_developer=request.user,
			        )
			        new_game.save()

			        #redirect to a new URL:
			        return HttpResponseRedirect('/gameshop/addGameSuccess')
			#if a GET (or any other method) we'll create a blank form
			else:
			    form = AddGameForm()

			template = loader.get_template('gameshop/addgame.html')
			context = RequestContext(request, {
					'form':form,
			    })
			return HttpResponse(template.render(context))
		else: #only developers can add games
			message = "Unable to add game - You have to be a developer to add games"
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))
	else: #only authorized users can do that action
		message = "Unable to add game - You have to login as a developer to add games"
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))

def addGameSuccess(request): #game successfully added
	message = "You successfully added game!"
	template = loader.get_template('gameshop/game-added-success.html')
	context = RequestContext(request, {
		'message':message,
	})
	return HttpResponse(template.render(context))

def changeGameDetails(request):#show interface for change game details
	if request.GET and request.GET['id']:#game id is sent
		if request.user.is_authenticated() and request.user.has_perm('gameshop.can_add'): #checks if user is a developer
			game_id = request.GET['id']
			if Games.dev_has_game(request.user, game_id): #checks if developer has that game
				game = Games.objects.get(pk=game_id)
				form = AddGameForm(instance=game) #load the form with game data
				template = loader.get_template('gameshop/changeGameDetails.html')
				context = RequestContext(request, {
						'form':form,
						'game_id': game_id,
				    })
				return HttpResponse(template.render(context))
			else: #developer does not have that game
				message = "You can't do this unauthorized action"
				template = loader.get_template('gameshop/sorry_page.html')
				context = RequestContext(request, {
						'message':message,
				    })
				return HttpResponse(template.render(context))
		else: #user is not a developer
			message = "You have to login as a developer to change game details"
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))

	else: #game id has not been sent
		message = "Please try again the last action."
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))

def updateGame(request): #update of the game details by developer
	if request.user.is_authenticated():
		if request.user.has_perm('gameshop.can_add'):
			if request.method == 'POST':
				#create a form instance and populate it with data from the request
				form = AddGameForm(request.POST)
				#check whether it's valid
				if form.is_valid():#process the data in form.cleaned as required
				#saving added game
					if Games.dev_has_game(request.user, request.POST['game_id']): #checks if developer has that game
						game_to_update = Games.objects.get(pk=request.POST['game_id'])
						game_to_update.game_name=form.cleaned_data['game_name']
						game_to_update.game_desc=form.cleaned_data['game_desc']
						game_to_update.game_link=form.cleaned_data['game_link']
						game_to_update.game_category=form.cleaned_data['game_category']
						game_to_update.game_price=form.cleaned_data['game_price']
						game_to_update.save()

						return HttpResponseRedirect('/gameshop/addGameSuccess')
					else: #that's not developer's game
						message = "Unauthorized action!"
						template = loader.get_template('gameshop/sorry_page.html')
						context = RequestContext(request, {
								'message':message,
						    })
						return HttpResponse(template.render(context))
			else:
				message = "Please try again the last action."
				template = loader.get_template('gameshop/sorry_page.html')
				context = RequestContext(request, {
						'message':message,
				    })
				return HttpResponse(template.render(context))
		else: #only developers can update games
			message = "Unable to update game - You have to be a developer to do that action"
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))
	else: #only authorized users can do that action
		message = "Unable to update game - You have to login as a developer"
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))

def deleteGame(request): #deletion of game by developer
	if request.GET and request.GET['id']: #game id is sent
		game_id =  request.GET['id']
		if request.user.is_authenticated():
			if request.user.has_perm('gameshop.can_add') and Games.dev_has_game(request.user, game_id): #checks if developer has that game
				game_to_delete = Games.objects.get(pk=game_id)
				game_to_delete.delete()
				message = "Game was deleted"
				template = loader.get_template('gameshop/sorry_page.html')
				context = RequestContext(request, {
						'message':message,
				    })
				return HttpResponse(template.render(context))	
			elif PlayerInventory.has_game(request.user, game_id): #checks if player has that game
				game_to_delete = PlayerInventory.objects.filter(game=game_id).get(player=request.user)
				game_to_delete.delete()
				message = "Game was deleted"
				template = loader.get_template('gameshop/sorry_page.html')
				context = RequestContext(request, {
						'message':message,
				    })
				return HttpResponse(template.render(context))	
			else: #if a user does't have that game (i.e. unauthorized access to that action)
				message = "Game can't be deleted. You don't have this game"
				template = loader.get_template('gameshop/sorry_page.html')
				context = RequestContext(request, {
						'message':message,
				    })
				return HttpResponse(template.render(context))	
		else: #only authorized users can do that action
			message = "You have to login to delete games"
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))
	else: #game id was not sent
		message = "Please try again the last action."
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))

def myAccount(request): #showing account page
	if request.user.is_authenticated(): 
		if request.user.has_perm('gameshop.can_add'): #redirect developers to corresponding view
			return HttpResponseRedirect('/gameshop/devAccount')
		else: #redirect players to corresponding view
			return HttpResponseRedirect('/gameshop/playerAccount')
	else:
		message = "You have to login to access that page"
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))

def devAccount(request): #showing developer's account page
	if request.user.is_authenticated() and request.user.has_perm('gameshop.can_add'):
		if request.GET.get('category'): #in case if a user wants to see games under specific category
			category = request.GET.get('category')
			developer_games = Games.objects.filter(game_developer__id=request.user.id).filter(game_category=category)
		else: #show all games available if no category selected
			developer_games = Games.objects.filter(game_developer__id=request.user.id)
		
		played_games = PlayerInventory.objects.filter(player=request.user)

		number_of_downloads = 0
		earnings = 0
		all_developer_games = Games.objects.filter(game_developer__id=request.user.id)
		for dev_game in all_developer_games: #get overall downloads and sellings
			earnings += dev_game.game_price*dev_game.game_number_downloads
			number_of_downloads += dev_game.game_number_downloads

		template = loader.get_template('gameshop/developer.html')
		context = RequestContext(request, {
				"list_of_games": developer_games,
				"number_of_downloads": number_of_downloads,
				"earnings": earnings,
				"list_of_played_games": played_games,
				"categories": Games.get_categories(),
			})
		return HttpResponse(template.render(context))
	else:
		message = "You have to login as a developer to access that page"
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))
 
def playerAccount(request): #showing player's account page
	if request.user.is_authenticated():
		if request.GET.get('category'): #in case if a user wants to see games under specific category
			category = request.GET.get('category')
			player_games = PlayerInventory.objects.filter(player=request.user).filter(game__game_category=category)
		else: #show all games available if no category selected
			player_games = PlayerInventory.objects.filter(player=request.user)

		template = loader.get_template('gameshop/gamer.html')
		context = RequestContext(request, {
				"list_of_games":player_games,
				"categories": Games.get_categories(),
			})
		return HttpResponse(template.render(context))
	else:
		message = "You have to login to access that page"
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))

def gameView(request):
	if request.GET and request.GET['id']: #game id is sent
		game_id = request.GET['id']
		if request.user.is_authenticated() and request.user.has_perm('gameshop.can_add'): #redirect developer to corresponding game view page
			return HttpResponseRedirect('/gameshop/devGameView?id='+game_id)
		elif request.user.is_authenticated(): #redirect player to corresponding game view page
			return HttpResponseRedirect('/gameshop/gamerGameView?id='+game_id)
		else: #redirect guest corresponding game view page
			try: #check if such game exists in database
				game = Games.objects.get(id=game_id)
				template = loader.get_template('gameshop/game_view.html')
				context = RequestContext(request, {
						"game": game,
						"category_name": Games.get_category(game.game_category),
						"game_scores": PlayerInventory.get_top_scores(request, game_id, 2),
						"user": request.user,
					})
				return HttpResponse(template.render(context))
			except ObjectDoesNotExist:#alert of game does not exist
				message = "The game does not exist."
				template = loader.get_template('gameshop/sorry_page.html')
				context = RequestContext(request, {
						'message':message,
				    })
				return HttpResponse(template.render(context))
	else: #game was not sent
		message = "Please try again the last action."
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))

def gamerGameView(request): #show player's game view
	if request.GET and request.GET['id']: #game id is sent
		game_id = request.GET['id']
		if request.user.is_authenticated():
			try: #check if such game exists in database
				game = Games.objects.get(id=game_id)
			except ObjectDoesNotExist: #alert of game does not exist
				message = "The game does not exist."
				template = loader.get_template('gameshop/sorry_page.html')
				context = RequestContext(request, {
						'message':message,
				    })
				return HttpResponse(template.render(context))
			if PlayerInventory.has_game(request.user, game_id): #Player has that game
				template = loader.get_template('gameshop/game_user_view.html')
				context = RequestContext(request, {
						"game": game,
						"category_name": Games.get_category(game.game_category),
						"game_scores": PlayerInventory.get_top_scores(request, game_id, 2),
						"user": request.user,
						"can_play": True,
						"request": request,
					})
				return HttpResponse(template.render(context))
			else: #Player has that game
				template = loader.get_template('gameshop/game_user_view.html')
				context = RequestContext(request, {
						"game": game,
						"category_name": Games.get_category(game.game_category),
						"game_scores": PlayerInventory.get_top_scores(request, game_id, 2),
						"user": request.user,
						"can_play": False,
						"request": request,
					})
				return HttpResponse(template.render(context))
		else:
			message = "You have to login to access that page"
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))							
	else: #game id was not sent
		message = "Please try again the last action."
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))

def devGameView(request): #show developer's game view
	if request.user.is_authenticated():
		if request.GET and request.GET['id']: #game id is sent
			if request.user.has_perm('gameshop.can_add'): #checks if user is a developer
				game_id = request.GET['id']
				if Games.dev_has_game(request.user, game_id): #checks if develoepr has that game
					game = Games.objects.get(pk=game_id)
					template = loader.get_template('gameshop/game_developer_view.html')
					context = RequestContext(request, {
							"game": game,
							"category_name": Games.get_category(game.game_category),
							"earnings": game.game_price * game.game_number_downloads,
							"has_game": True,
						})
					return HttpResponse(template.render(context))
				else: #redirect if not
					return HttpResponseRedirect('/gameshop/gamerGameView?id='+game_id)
			else:
				message = "You have to login as a developer to access that page"
				template = loader.get_template('gameshop/sorry_page.html')
				context = RequestContext(request, {
						'message':message,
				    })
				return HttpResponse(template.render(context))
		else: #game id was not sent
			message = "Please try again the last action."
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))			
	else:
		message = "You have to login as a developer to access that page"
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))

def playGame(request):
	if request.user.is_authenticated() and request.GET['id']: #if game id is sent
		game_id = request.GET['id']
		if request.user.has_perm('gameshop.can_add') and Games.dev_has_game(request.user, game_id): #develoepr can't play his own games
			message = "You can't play the game you developed."
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))
		elif PlayerInventory.has_game(request.user, game_id): #player can play only games from his inventory
			game = Games.objects.get(pk=game_id)
			inventoryData = PlayerInventory.objects.filter(player=request.user).get(game=game)
			template = loader.get_template('gameshop/play_game.html')
			context = RequestContext(request, {
					"game": game,
					"inventoryData": inventoryData,
				})
			return HttpResponse(template.render(context))
		else:
			message = "You didn't buy that game to play it."
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))
	else: #game id was not sent
		message = "Please try again the last action."
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))

def saveGame(request):
	if request.user.is_authenticated() and request.GET['gstate']:
		game_id =  request.GET['game_id']
		if PlayerInventory.has_game(request.user, game_id): #player can save only games from his inventory
			gstate = request.GET['gstate']
			game_score =  request.GET['game_score']
			game_instant = Games.objects.get(pk=game_id)
			game_item = PlayerInventory.objects.filter(player=request.user).get(game=game_instant)
			game_item.game_state = gstate
			if float(game_score)>game_item.max_score:
				game_item.max_score = game_score
			game_item.save()
			return HttpResponse("Game saved")
		else:
			message = "You don't have that game in your inventory. You can't save it."
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))

def loadGame(request):
	if request.user.is_authenticated() and request.GET['game_id']:
		game_id =  request.GET['game_id']
		if PlayerInventory.has_game(request.user, game_id): #player can load only games from his inventory
			game_instant = Games.objects.get(pk=game_id)
			game_item = PlayerInventory.objects.filter(player=request.user).get(game=game_instant)
			gstate = game_item.game_state
			if gstate:
				return HttpResponse(gstate)
			else:
				return HttpResponse("none")
		else:			
			message = "You don't have that game in your inventory. You can't load it."
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))

def compareScore(request):
	if request.user.is_authenticated() and request.GET['game_id']:
		game_id =  request.GET['game_id']
		if PlayerInventory.has_game(request.user, game_id): #only if player has game
			game_score =  request.GET['game_score']
			game_instant = Games.objects.get(pk=game_id)
			game_item = PlayerInventory.objects.filter(player=request.user).get(game=game_instant)
			if float(game_score)>game_item.max_score:
				game_item.max_score = game_score
				game_item.save()
				return HttpResponse("New score!")
			else:
				return HttpResponse("none")
		else:
			return HttpResponse("Unauthorized action!")					

def downloads(request): #show developer's statistics in a table
	if request.user.is_authenticated() and request.user.has_perm('gameshop.can_add'):
		try: #only if there are any games
			developer_games = Games.objects.filter(game_developer=request.user)
			template = loader.get_template('gameshop/downloads.html')
			context = RequestContext(request, {
					'developer_games':developer_games,
					'categories': Games.CATEGORY_NAMES,
			    })
			return HttpResponse(template.render(context))
		except ObjectDoesNotExist: #esle inform about no games
			message = "You don't have any games to show"
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))			
	else:
		message = "You have to login as a developer to access that page"
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))

def myGameResults(request): #show player's game statistics in a table
	if request.user.is_authenticated():
		try: #only if there are any games
			player_games = PlayerInventory.objects.filter(player=request.user)
			template = loader.get_template('gameshop/myGameResults.html')
			context = RequestContext(request, {
					'player_games': player_games,
					'categories': Games.CATEGORY_NAMES,
			    })
			return HttpResponse(template.render(context))
		except ObjectDoesNotExist:
			message = "You don't have any games to show"
			template = loader.get_template('gameshop/sorry_page.html')
			context = RequestContext(request, {
					'message':message,
			    })
			return HttpResponse(template.render(context))			
	else: #esle inform about no games
		message = "You have to login to access that page"
		template = loader.get_template('gameshop/sorry_page.html')
		context = RequestContext(request, {
				'message':message,
		    })
		return HttpResponse(template.render(context))		
