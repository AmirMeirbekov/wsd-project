from django.conf.urls import patterns, url

from gameshop import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^home$', views.index),
	url(r'^about$', views.about),
	url(r'^addGame$', views.addGame),
	url(r'^addGameSuccess$', views.addGameSuccess),
	url(r'^myAccount$', views.myAccount),
	url(r'^devAccount$', views.devAccount),
	url(r'^playerAccount$', views.playerAccount),
	url(r'^devGameView$', views.devGameView),
	url(r'^gamerGameView$', views.gamerGameView),
	url(r'^gameView$', views.gameView),
	url(r'^playGame$', views.playGame),
	url(r'^saveGame$', views.saveGame),
	url(r'^loadGame$', views.loadGame),
	url(r'^deleteGame$', views.deleteGame),
	url(r'^updateGame$', views.updateGame),
	url(r'^compareScore$', views.compareScore),
	url(r'^changeGameDetails$', views.changeGameDetails),
	url(r'^downloads$', views.downloads),
	url(r'^myGameResults$', views.myGameResults),
)