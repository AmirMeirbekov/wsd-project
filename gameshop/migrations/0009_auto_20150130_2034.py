# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0008_remove_devinventory_developer'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='devinventory',
            name='game',
        ),
        migrations.DeleteModel(
            name='DevInventory',
        ),
        migrations.RenameField(
            model_name='playerinventory',
            old_name='game',
            new_name='game_name',
        ),
        migrations.AddField(
            model_name='playerinventory',
            name='game_state',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='playerinventory',
            name='max_score',
            field=models.FloatField(null=True),
            preserve_default=True,
        ),
    ]
