# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0003_auto_20150128_1623'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='devinventory',
            name='id',
        ),
        migrations.AlterField(
            model_name='devinventory',
            name='game',
            field=models.OneToOneField(to='gameshop.Games', serialize=False),
            preserve_default=True,
        ),
    ]
