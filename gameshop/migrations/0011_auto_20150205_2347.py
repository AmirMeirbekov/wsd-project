# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0010_auto_20150130_2042'),
    ]

    operations = [
        migrations.AlterField(
            model_name='games',
            name='game_category',
            field=models.CharField(choices=[('L', 'Logical'), ('S', 'Strategies'), ('B', 'Board games')], max_length=1),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='playerinventory',
            name='game_state',
            field=models.TextField(blank=True, null=True),
            preserve_default=True,
        ),
    ]
