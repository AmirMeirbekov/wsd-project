# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0002_auto_20150128_1604'),
    ]

    operations = [
        migrations.AlterField(
            model_name='devinventory',
            name='earnings',
            field=models.FloatField(default=0),
            preserve_default=True,
        ),
    ]
