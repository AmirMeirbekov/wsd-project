# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0009_auto_20150130_2034'),
    ]

    operations = [
        migrations.RenameField(
            model_name='playerinventory',
            old_name='game_name',
            new_name='game',
        ),
        migrations.AlterField(
            model_name='playerinventory',
            name='game_state',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='playerinventory',
            name='max_score',
            field=models.FloatField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
