# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0007_auto_20150128_2142'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='devinventory',
            name='developer',
        ),
    ]
