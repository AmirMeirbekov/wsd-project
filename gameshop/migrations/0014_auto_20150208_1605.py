# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0013_auto_20150206_0423'),
    ]

    operations = [
        migrations.AlterField(
            model_name='games',
            name='game_last_download',
            field=models.DateField(null=True),
            preserve_default=True,
        ),
    ]
