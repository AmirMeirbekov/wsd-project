# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0015_auto_20150208_1630'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scores',
            name='game',
        ),
        migrations.RemoveField(
            model_name='scores',
            name='player',
        ),
        migrations.DeleteModel(
            name='Scores',
        ),
    ]
