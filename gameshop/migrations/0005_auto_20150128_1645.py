# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0004_auto_20150128_1641'),
    ]

    operations = [
        #migrations.AlterField(
         #   model_name='devinventory',
          #  name='game',
         #   field=models.ForeignKey(unique=True, to='gameshop.Games'),
           # preserve_default=True,
        #),
        migrations.RemoveField(
            model_name='devinventory',
            name='game',
        ),
        migrations.AddField(
            model_name='devinventory',
            name='id',
            field=models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False),
            preserve_default=False,
        ),
    ]
