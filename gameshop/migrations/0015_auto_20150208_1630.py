# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0014_auto_20150208_1605'),
    ]

    operations = [
        migrations.RenameField(
            model_name='games',
            old_name='game_pub_data',
            new_name='game_pub_date',
        ),
    ]
