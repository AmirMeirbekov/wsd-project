# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0012_auto_20150206_0357'),
    ]

    operations = [
        migrations.AlterField(
            model_name='playerinventory',
            name='max_score',
            field=models.FloatField(blank=True, default=0),
            preserve_default=True,
        ),
    ]
