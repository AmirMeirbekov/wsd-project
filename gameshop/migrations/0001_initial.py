# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='DevInventory',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('earnings', models.FloatField()),
                ('developer', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Games',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('game_name', models.CharField(max_length=50)),
                ('game_desc', models.TextField()),
                ('game_link', models.URLField()),
                ('game_pub_data', models.DateField(auto_now_add=True)),
                ('game_number_downloads', models.IntegerField()),
                ('game_price', models.FloatField()),
                ('game_last_download', models.DateField(auto_now_add=True)),
                ('game_category', models.CharField(choices=[('G', 'Good'), ('N', 'Normal'), ('B', 'Bad')], max_length=1)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PLayerInventory',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('max_score', models.FloatField()),
                ('game', models.ForeignKey(to='gameshop.Games')),
                ('player', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Scores',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, primary_key=True, verbose_name='ID')),
                ('max_score', models.FloatField()),
                ('game', models.ForeignKey(to='gameshop.Games')),
                ('player', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='devinventory',
            name='game',
            field=models.ForeignKey(to='gameshop.Games'),
            preserve_default=True,
        ),
    ]
