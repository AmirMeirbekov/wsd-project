# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0016_auto_20150213_0038'),
    ]

    operations = [
        migrations.AddField(
            model_name='playerinventory',
            name='order_num',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
