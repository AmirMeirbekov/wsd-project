# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0005_auto_20150128_1645'),
    ]

    operations = [
        migrations.AddField(
            model_name='devinventory',
            name='g',
            field=models.OneToOneField(to='gameshop.Games', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='games',
            name='game_last_download',
            field=models.DateField(auto_now_add=True, null=True),
            preserve_default=True,
        ),
    ]
