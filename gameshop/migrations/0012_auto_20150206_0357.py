# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0011_auto_20150205_2347'),
    ]

    operations = [
        migrations.AlterField(
            model_name='games',
            name='game_link',
            field=models.CharField(max_length=200),
            preserve_default=True,
        ),
    ]
