# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gameshop', '0006_auto_20150128_2126'),
    ]

    operations = [
        migrations.RenameField(
            model_name='devinventory',
            old_name='g',
            new_name='game',
        ),
    ]
