$(document).ready(function(){
	
	var mainCanvas = $("#canvas")[0];
	var context = mainCanvas.getContext("2d");
	var w = $("#canvas").width();
	var h = $("#canvas").height();

	var snakeCells;

	var cellWidth = 15;
	var food;
	var score;
	var direction;

	var pause;
	var game_interval = 120;

	function food_intersects_snake(x, y)
	{
		for(var i = 0; i < snakeCells.length; i++)
		{
			var c = snakeCells[i];

			if (c.x == x && c.y == y)
				return true;
		}
		return false;
	}

	// Init methods

	function init_food () {

		var new_food_x = Math.round(Math.random()*(w-cellWidth)/cellWidth);
		var new_food_y = Math.round(Math.random()*(h-cellWidth)/cellWidth);

		while (food_intersects_snake(new_food_x, new_food_y))
		{
			new_food_x = Math.round(Math.random()*(w-cellWidth)/cellWidth);
			new_food_y = Math.round(Math.random()*(h-cellWidth)/cellWidth);
		}
		
		// Init food at random position
		food = {
			x: new_food_x,
			y: new_food_y, 
		};
	}

	function init_game()
	{
		// Init snake
		var length = 5; //snake's Length
		snakeCells = []; 
		direction = "right";	//by default
		score = 0;

		for(var i = length-1; i>=0; i--)
		{
			snakeCells.push({x: i, y:0});
		}

		init_food();
		// set up a timer
		if(typeof game_loop != "undefined") clearInterval(game_loop);

		pause = true;
		game_interval = 120;
		draw_ui();
		//
		//paint();
	}

	init_game();

	function load_state()
	{
		var msg = {
        "messageType": "LOAD_REQUEST",
      };
      window.parent.postMessage(msg, "*");
	}

	function save_state()
	{
		var msg = {
        "messageType": "SAVE",
        "gameState": {
          "snakeItems": snakeCells,
          "foodItem": food,
          "score": score,
          "direction": direction
        }
      };
      window.parent.postMessage(msg, "*");
	}

	$("#loadbutton").click( function () {

		load_state();
	});

	$("#savebutton").click( function () {

		save_state();
	});

	window.addEventListener("message", function(evt) {
      if(evt.data.messageType === "LOAD") {

      	pause = true;

        snakeCells = evt.data.gameState.snakeItems;
        score = evt.data.gameState.score;
        food = evt.data.gameState.foodItem;
        direction = evt.data.gameState.direction;

        draw_ui();

      }
    });

	function report_score()
	{
		var msg = {
        "messageType": "SCORE",
        "score": score
      };
      window.parent.postMessage(msg, "*");
	}

	function draw_ui()
	{
		// Fill bg and strokes
		context.fillStyle = "black";
		context.fillRect(0, 0, w, h);
		context.strokeStyle = "black";
		context.strokeRect(0, 0, w, h);

		for(var i = 0; i < snakeCells.length; i++)
		{
			var c = snakeCells[i];
			
			paint_cell(c.x, c.y);
		}
		
		
		paint_cell(food.x, food.y);

		$("#pscore").text(score);
	}

	function paint()
	{
		
		var new_x = snakeCells[0].x;
		var new_y = snakeCells[0].y;

		var max_x = w/cellWidth;
		var max_y = h/cellWidth;
		
		if(direction == "right")
		{ 
			new_x = (new_x + 1) % max_x;
		}
		else if(direction == "left") 
		{
			new_x--;
			if (new_x == -1)
				new_x = max_x-1;
		}
		else if(direction == "up") 
		{
			new_y--;
			if (new_y == -1)
				new_y = max_y-1;
		}
		else if(direction == "down") 
		{
			new_y = (new_y + 1) % max_y;
		}
		
		// Check for game over
		//if(new_x == -1 || new_x == w/cellWidth || new_y == -1 || new_y == h/cellWidth || check_collision(new_x, new_y, snakeCells))
		if (check_collision(new_x, new_y, snakeCells))
		{
			report_score();

			alert("Game over! your score: " + score);
			//restart game
			init_game();
			return;
		}
		
		// Check for food 
		if(new_x == food.x && new_y == food.y)
		{
			var tail = {x: new_x, y: new_y};
			score++;

			init_food();

			game_interval -= 5;
			clearInterval(game_loop);
			game_loop = setInterval(paint, game_interval);
		}
		else
		{
			var tail = snakeCells.pop(); //pop the last snake cell

			tail.x = new_x; tail.y = new_y;
		}
		
		snakeCells.unshift(tail); //insert last cell instead of head
		
		draw_ui();
		
		// var score_txt = "Your score: " + score;
		// context.font="20px Georgia";
		// context.fillText(score_txt, 10, h-10);
	}
	
	// Paint function
	function paint_cell(x, y)
	{
		if (food.x == x && food.y == y)
		{
			context.fillStyle = "yellow";
			context.fillRect(x*cellWidth, y*cellWidth, cellWidth, cellWidth);
			context.strokeStyle = "black";
			context.strokeRect(x*cellWidth, y*cellWidth, cellWidth, cellWidth);
		} else 
		{
			if (x == snakeCells[0].x && y == snakeCells[0].y)
			{
				context.fillStyle = "#008000";
				context.fillRect(x*cellWidth, y*cellWidth, cellWidth, cellWidth);
				context.strokeStyle = "black";
				context.strokeRect(x*cellWidth, y*cellWidth, cellWidth, cellWidth);
			} else
			{
				context.fillStyle = "white";
				context.fillRect(x*cellWidth, y*cellWidth, cellWidth, cellWidth);
				context.strokeStyle = "black";
				context.strokeRect(x*cellWidth, y*cellWidth, cellWidth, cellWidth);
			}
		}
	}
	
	function check_collision(x, y, array)
	{
		for(var i = 0; i < array.length-1; i++)
		{
			if(array[i].x == x && array[i].y == y)
			 return true;
		}
		return false;
	}
	
	// Listen for keyboad
	$(document).keydown(function(e){
		var key = e.which;

		if(key == "32")
		{
			if (pause)
			{
				game_loop = setInterval(paint, game_interval);
			} else {
				clearInterval(game_loop);
			}

			pause = !pause;

		}

		if (pause)
			return;
		
		if(key == "37" && direction != "right") direction = "left";
		else if(key == "38" && direction != "down") direction = "up";
		else if(key == "39" && direction != "left") direction = "right";
		else if(key == "40" && direction != "up") direction = "down";
		
	})

})
