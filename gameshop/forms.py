import re
from django import forms
from django.forms import ModelForm, Textarea, TextInput, URLInput, NumberInput, Select

#from django.contrib.auth.models import User
from gameshop.models import Games
from django.utils.translation import ugettext_lazy as _


class AddGameForm(ModelForm):
	class Meta:
		model = Games
		fields = ['game_name', 'game_desc', 'game_link', 'game_price', 'game_category']
		widgets = {
			'game_name': TextInput(attrs={'class':'form-control'}),
			'game_link': TextInput(attrs={'class':'form-control', 'value':'snake/index.html'}),
			'game_price': NumberInput(attrs={'class':'form-control'}),
			'game_desc': Textarea(attrs={'cols': 60, 'rows': 10}),
			'game_category': Select(attrs={'class':'form-control'}),
		}
		labels = {
			'game_desc': _('Games description')
		}